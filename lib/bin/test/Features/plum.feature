#Author: Juan Trujillo Salas
#PlumChallenge
#Date: 22 september 2022
Feature: This feature is about plum challenge automation

  Scenario Outline: Succesfull account creation
    Given Actor opens navigator and select the country and language
    When Actor fill the form for account creation with <typeZipCode>, <name>, <lName>, <sEmail>, <user>, <pwd>, <cPwd>
    Then Actor displays the successfull account creation

    Examples: 
      | typeZipCode | name | lName    | sEmail                | user      | pwd       | cPwd      |
      |       07512 | Juan | Trujillo | jmatru625@hotmail.com | jmatru634 | Plum@2022 | Plum@2022 |
