package plum.definitions;

import java.awt.AWTException;

import org.openqa.selenium.WebDriver;
import plum.pages.PlumLocators;
import plum.steps.Connection;
import cucumber.api.java.en.*;

public class StepsDefinitions {

	private WebDriver driver;
	private Connection connection = new Connection();
	private PlumLocators plumLocators = new PlumLocators(driver);

	@Given("^Actor opens navigator and select the country and language$")
	public void openNavigator() throws InterruptedException {
		this.connection = new Connection();
		this.driver = this.connection.openNavigator();
		this.plumLocators = new PlumLocators(driver);
		plumLocators.selectCountryLanguage();
	}

	@When("^Actor fill the form for account creation with (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
	public void accountCreations(String typeZipCode, String name, String lName, String sEmail, String user, String pwd,
			String cPwd) throws InterruptedException, AWTException {
		this.plumLocators = new PlumLocators(driver);
		plumLocators.accountCreation(typeZipCode, name, lName, sEmail, user, pwd, cPwd);
	}

	@Then("^Actor displays the successfull account creation$")
	public void successfullCreation() throws InterruptedException, AWTException {
		this.plumLocators = new PlumLocators(driver);
		plumLocators.successfullUserCreation();

	}
}
