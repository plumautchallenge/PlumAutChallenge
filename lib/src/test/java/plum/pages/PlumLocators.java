package plum.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PlumLocators {

	@FindBy(how = How.XPATH, using = "//option[contains(text(),'Canada')]")
	private WebElement selectCountry;

	@FindBy(how = How.XPATH, using = "//option[contains(text(),'English')]")
	private WebElement selectLanguage;

	@FindBy(how = How.ID, using = "languageSubmit")
	private WebElement submitLanguage;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log In')]")
	private WebElement btnLogin;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Create an account')]")
	private WebElement btnCreateAcccount;

	@FindBy(how = How.XPATH, using = "//body/div[5]/div[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElement zipCode;

	@FindBy(how = How.XPATH, using = "//body/div[5]/div[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]")
	private WebElement btnSearchZipCode;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[5]/div[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[2]/div[5]/div[2]/a[2]")
	private WebElement selectThirdStylist;

	@FindBy(how = How.ID, using = "Customer_FirstName")
	private WebElement firstName;

	@FindBy(how = How.ID, using = "Customer_LastName")
	private WebElement lastName;

	@FindBy(how = How.ID, using = "Customer_Email")
	private WebElement email;

	@FindBy(how = How.ID, using = "Customer_LoginName")
	private WebElement userName;

	@FindBy(how = How.ID, using = "Password")
	private WebElement password;

	@FindBy(how = How.ID, using = "ConfirmPassword")
	private WebElement confirmPwd;

	@FindBy(how = How.XPATH, using = "/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]")
	private WebElement robotCheck;

	@FindBy(how = How.XPATH, using = "//button[@id='submit']")
	private WebElement btnSubmit;

	public PlumLocators(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void selectCountryLanguage() throws InterruptedException {
		Thread.sleep(1000);
		selectCountry.click();
		selectLanguage.click();
		submitLanguage.click();
	}

	public void accountCreation(String typeZipCode, String name, String lName, String sEmail, String user, String pwd,
			String cPwd) throws InterruptedException, AWTException {
		btnLogin.click();
		btnCreateAcccount.click();
		zipCode.sendKeys(typeZipCode);
		btnSearchZipCode.click();
		Thread.sleep(2000);
		selectThirdStylist.click();
		Thread.sleep(2000);
		firstName.sendKeys(name);
		lastName.sendKeys(lName);
		email.sendKeys(sEmail);
		userName.sendKeys(user);
		password.sendKeys(pwd);
		confirmPwd.sendKeys(cPwd);

	}
	@SuppressWarnings("deprecation")
	public void successfullUserCreation() throws AWTException, InterruptedException {
		Robot robot = new Robot();
		robot.mouseWheel(2);
		robot.mouseMove(220, 480);
		Thread.sleep(1000);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
		Thread.sleep(8000);
		btnSubmit.click();
	}

}
