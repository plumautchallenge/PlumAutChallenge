package plum.steps;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.Before;

public class Connection {
	
	private WebDriver driver;

	@Before
	public WebDriver openNavigator() {
		System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.colorstreet.com/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return this.driver;
	}

}
